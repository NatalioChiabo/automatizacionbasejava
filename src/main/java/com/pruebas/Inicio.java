package com.pruebas;

import com.automatizacionbase.*;

public class Inicio {
    public static void main(String[] args) throws NoSuchMethodException, SecurityException {
        //Describir todas las pruebas a correr
        Prueba[] pruebasACorrer = new Prueba[]{
            new Prueba(
                "true=false",
                "descripcion descriptiva que describe de forma descriptante las descripciones que describen describiendo descriptivamente",
                Inicio.class.getMethod("test"))//Esta es la forma en la que se indica el metodo a ejecutar para ejecutar la prueba
                //(La clase donde se encuentra el metodo).class.getMethod("nombre del metodo por string")
                //Si hay una mejor forma, avisenme
        };
        //Correr las pruebas
        ReportadorDePruebas.CorrerPruebas("Prueba Total", pruebasACorrer,false,true);
        //No se ha testeado la ejecucion en paralelo
        //No se ha implementado el envio por mail
    }

    public static void test() throws PruebaFalladaException{
        throw new PruebaFalladaException();
    }
}
