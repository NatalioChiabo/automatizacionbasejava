package com.automatizacionbase;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.*;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.*;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerDriverService;
import org.openqa.selenium.ie.InternetExplorerOptions;

public class Driver {

    public static String url = "https://www.google.com/";

    
	private static final String TIPO_DRIVER = "webdriver.chrome.driver";
	private static final String PATH_DRIVER = "./src/test/resources/drivers/googlechrome/chromedriver2.exe";
	

    public static WebDriver driver;

    //public static AlSalir alTerminarLaPrueba;

    public static Dimension window_size = new Dimension(0, 0);

    public static String directorio_descarga = System.getProperty("user.dir") + "Descargas";

    public static void IniciarChromeDriver()
    {
        ReportadorDePruebas.secuenciaDePasos.AbrirPaso("Inicializar ChromeDriver");

        System.setProperty(TIPO_DRIVER, PATH_DRIVER);

        //Todo esto es una configuracon para permitir descargar archivos sin que te salte un mensaje
        ChromeOptions chrome_options = new ChromeOptions();
        chrome_options.addArguments("--safebrowsing-disable-download-protection");
        chrome_options.addArguments("safebrowsing-disable-extension-blacklist");
        chrome_options.addArguments("--disable-web-security");
        chrome_options.addArguments("--ignore-certificate-errors");
        chrome_options.addArguments("--no-sandbox");
        // chrome_options.AddUserProfilePreference("profile.default_content_setting_values.automatic_downloads", 1);
        // chrome_options.AddUserProfilePreference("profile.default_content_settings.popups", 0);
        // chrome_options.AddUserProfilePreference("disable-popup-blocking", "true");
        // chrome_options.AddUserProfilePreference("download.prompt_for_download", false);
        // chrome_options.AddUserProfilePreference("safebrowsing.enabled", false);
        // chrome_options.AddUserProfilePreference("download_restrictions", "0");
        // chrome_options.AddUserProfilePreference("download.default_directory", directorio_descarga);
        // //Esta opcion sirve para dejarle saber a chrome que tipo de archivos permitir
        // chrome_options.AddUserProfilePreference("download.extensions_to_open", "application/x-java-jnlp-file");

        ChromeDriver cd = new ChromeDriver(chrome_options);
        cd.manage().window().maximize();
        cd.navigate().to(url);

        driver = cd;

        driver.manage().deleteAllCookies();

        ReportadorDePruebas.secuenciaDePasos.CerrarPaso("ChromeDriver Inicializado");
    }

    public static void IniciarFireFoxDriver()
    {
        ReportadorDePruebas.secuenciaDePasos.AbrirPaso("Inicializar FireFoxDriver");

        FirefoxProfile profile = new FirefoxProfile();

        //Todo esto es una configuracon para permitir descargar archivos sin que te salte un mensaje
        profile.setPreference("browser.download.folderList", 2);
        profile.setPreference("browser.download.dir", directorio_descarga);
        profile.setPreference("browser.safebrowsing.downloads.enabled", false);
        profile.setPreference("browser.download.manager.focusWhenStarting", false);
        profile.setPreference("browser.download.useDownloadDir", true);
        profile.setPreference("browser.helperApps.alwaysAsk.force", false);
        profile.setPreference("browser.download.manager.alertOnEXEOpen", false);
        profile.setPreference("browser.download.manager.closeWhenDone", true);
        profile.setPreference("browser.download.manager.showAlertOnComplete", false);
        profile.setPreference("browser.download.manager.useWindow", false);
        //Esta opcion sirve para dejarle saber a firefox que tipo de archivos permitir
        profile.setPreference("browser.helperApps.neverAsk.saveToDisk", "application/octet-stream,text/xml,application/x-ms-application, application/x-java-jnlp-file");

        FirefoxOptions options = new FirefoxOptions();
        options.setProfile(profile);

        FirefoxDriver fd = new FirefoxDriver(options);
        fd.manage().window().maximize();
        window_size = fd.manage().window().getSize();


        driver = fd;

        driver.navigate().to(url);

        ReportadorDePruebas.secuenciaDePasos.CerrarPaso("FireFoxDriver Inicializado");
    }

    public static void IniciarEdgeDriver()
    {
        ReportadorDePruebas.secuenciaDePasos.AbrirPaso("Inicializar EdgeDriver");

        EdgeDriver ed = new EdgeDriver();
        ed.manage().window().maximize();
        window_size = ed.manage().window().getSize();
        ed.navigate().to(url);

        driver = ed;

        ReportadorDePruebas.secuenciaDePasos.CerrarPaso("EdgeDriver Inicializado");
    }

    public static void IniciarIEDriver() throws InterruptedException
    {
        ReportadorDePruebas.secuenciaDePasos.AbrirPaso("Inicializar InternetExplorerDriver");

        InternetExplorerDriverService service = InternetExplorerDriverService.createDefaultService();
        InternetExplorerOptions options = new InternetExplorerOptions();
        options.setPageLoadStrategy(PageLoadStrategy.EAGER);

        options.destructivelyEnsureCleanSession();
        options.introduceFlakinessByIgnoringSecurityDomains();

        InternetExplorerDriver ied = new InternetExplorerDriver(service, options);

        ied.manage().window().maximize();
        window_size = ied.manage().window().getSize();
        ied.navigate().to(url);

        driver = ied;

        Thread.sleep(Utils.longSleep);

        ReportadorDePruebas.secuenciaDePasos.CerrarPaso("InternetExplorerDriver Inicializado");
    }

    public static void Quit()
    {
        if(driver!=null)driver.quit();
    }

    public static void CerrarWebDriver()
    {
        Quit();

        AlTerminarPrueba();
    }

    public static void AlTerminarPrueba()
    {
        //alTerminarLaPrueba?.Invoke();
        //alTerminarLaPrueba = delegate { };
    }

    public static Boolean AcceptAlert()
    {
        try {
            if(driver!=null)driver.switchTo().alert().accept();
            if(driver!=null)driver.switchTo().defaultContent();
            Thread.sleep(Utils.shortSleep);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
