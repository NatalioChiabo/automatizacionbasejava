package com.automatizacionbase;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;

public class EmailManager {

    public static String email = "encodesatestingreportes@gmail.com";
    public static String pass = "9vA2uIP6mIpm";
    public static String recipiente;

    /// <summary>
    /// Este metodo genera un html cuando solo hay 1 prueba
    /// En el caso de ser fallida le agrega la imagen sacada al final de la prueba
    /// </summary>
    /// <returns>El documento html, como XmlDocument</returns>
    static String GenerarHTMLIndividual(String titulo,
        String fecha_hora,
        ResultadoPrueba prueba)
    {
        String doc = "";
        String filePath = "";
        
        if (prueba.resultado == ResultadoPrueba.Resultado.Aprobada)
        {
            filePath = System.getProperty("user.dir") + "\\HTML\\Email_Template_Individual_Sucess.html";
        }
        else if (prueba.resultado == ResultadoPrueba.Resultado.Fallada)
        {
            filePath = System.getProperty("user.dir") + "\\HTML\\Email_Template_Individual_Failure.html";
        }
        else if (prueba.resultado == ResultadoPrueba.Resultado.Alerta)
        {
            filePath = System.getProperty("user.dir") + "\\HTML\\Email_Template_Individual_Alert.html";
        }

        try {
            doc = Files.readString(Path.of(filePath));    
        } catch (IOException e) {
            System.out.println("IOException, returning");
            return "";
        }

        doc = doc
            .replace("{titulo}", Encode(titulo))
            .replace("{fecha_hora}", Encode(fecha_hora))
            .replace("{nombre_prueba}", Encode(prueba.nombre_prueba))
            .replace("{duracion}", Encode(prueba.duracion))
            .replace("{detalles_salida}", Encode(prueba.detalles_salida))
            .replace(System.lineSeparator(), "<br />")
            .replace("{imagen_prueba}", "cid:" + Encode(prueba.imagen_prueba));
        
        return doc;
    }


    /// <summary>
    /// Este metodo genera un html cuando solo hay mas de 1 prueba
    /// Le agrega la imagen obtenida a las pruebas fallidas
    /// </summary>
    /// <returns>El documento html, como XmlDocument</returns>
    public static String GenerarHTMLMultiple(
        String titulo,
        String fecha_hora,
        String grados_porcentaje_pasadas,
        String grados_porcentaje_falladas,
        String grados_porcentaje_alerta,
        String pruebas_pasadas,
        String pruebas_falladas,
        String pruebas_alerta,
        String pruebas_totales,
        String duracion_total,
        HashMap<Navegador, ResultadoPrueba[]> pruebas)
    {
        String doc = "";
        String nodoTestContainer = "";
        String nodoDetallesImagen = "";
        String nodoDetallesAprobada = "";
        String nodoBrowserContainer = "";
        try {
            doc = Files.readString(Path.of(System.getProperty("user.dir")                  + "\\HTML\\Email_Template_Multiple.html"));
            nodoTestContainer = Files.readString(Path.of(System.getProperty("user.dir")    + "\\HTML\\Email_Template_Part_testcontainer.html"));
            nodoDetallesImagen = Files.readString(Path.of(System.getProperty("user.dir")   + "\\HTML\\Email_Template_Part_imagedetails.html"));
            nodoDetallesAprobada = Files.readString(Path.of(System.getProperty("user.dir") + "\\HTML\\Email_Template_Part_succesdetail.html"));
            nodoBrowserContainer = Files.readString(Path.of(System.getProperty("user.dir") + "\\HTML\\Email_Template_Part_bowsercontainer.html"));
        } catch (IOException e) {
            System.out.println("IOException, returning");
            return "";
        }

        doc = doc
            .replace("{titulo}", Encode(titulo))
            .replace("{fecha_hora}", Encode(fecha_hora))
            .replace("{porcentaje_pasadas}", Encode(grados_porcentaje_pasadas))
            .replace("{porcentaje_falladas}", Encode(grados_porcentaje_falladas))
            .replace("{porcentaje_alerta}", Encode(grados_porcentaje_alerta))
            .replace("{pruebas_pasadas}", Encode(pruebas_pasadas))
            .replace("{pruebas_falladas}", Encode(pruebas_falladas))
            .replace("{pruebas_alerta}", Encode(pruebas_alerta))
            .replace("{pruebas_totales}", Encode(pruebas_totales))
            .replace("{duracion_total}", Encode(duracion_total));

        String browser_list = "";
        if (pruebas.containsKey(Navegador.Chrome))
        {
            String chrome_broser = nodoBrowserContainer;
            chrome_broser = chrome_broser.replace("{browser_Icon}", "https://icons.iconarchive.com/icons/google/chrome/256/Google-Chrome-icon.png")
            .replace("{browser_name}", "Chrome");
            String test_list = "";

            for (int i = 0; i < pruebas.get(Navegador.Chrome).length; i++)
            {
                test_list = test_list.concat(CrearNodo(nodoTestContainer, nodoDetallesImagen, nodoDetallesAprobada, pruebas.get(Navegador.Chrome)[i]));
            }
            chrome_broser = chrome_broser.replace("{test_list}", test_list);
            browser_list = browser_list.concat(chrome_broser);
        }

        if (pruebas.containsKey(Navegador.Firefox))
        {
            String firefox_broser = nodoBrowserContainer;
            firefox_broser = firefox_broser.replace("{browser_Icon}", "https://d33wubrfki0l68.cloudfront.net/06185f059f69055733688518b798a0feb4c7f160/9f07a/images/product-identity-assets/firefox.png")
            .replace("{browser_name}", "Firefox");
            String test_list = "";

            for (int i = 0; i < pruebas.get(Navegador.Firefox).length; i++)
            {
                test_list = test_list.concat(CrearNodo(nodoTestContainer, nodoDetallesImagen, nodoDetallesAprobada, pruebas.get(Navegador.Firefox)[i]));
            }
            firefox_broser = firefox_broser.replace("{test_list}", test_list);
            browser_list = browser_list.concat(firefox_broser);
        }

        if (pruebas.containsKey(Navegador.IE))
        {
            String ie_broser = nodoBrowserContainer;
            ie_broser = ie_broser.replace("{browser_Icon}", "https://upload.wikimedia.org/wikipedia/commons/1/1f/Internet_Explorer_10%2B11_computer_icon.png")
            .replace("{browser_name}", "IE");
            String test_list = "";

            for (int i = 0; i < pruebas.get(Navegador.IE).length; i++)
            {
                test_list = test_list.concat(CrearNodo(nodoTestContainer, nodoDetallesImagen, nodoDetallesAprobada, pruebas.get(Navegador.IE)[i]));
            }
            ie_broser = ie_broser.replace("{test_list}", test_list);
            browser_list = browser_list.concat(ie_broser);
        }
        
        doc = doc.replace("{browser_list}", browser_list);

        return doc;
    }

    static String CrearNodo(String testContainer, String detallesImagen, String detalleAprobada, ResultadoPrueba prueba)
    {
        switch (prueba.resultado)
        {
            case Aprobada:
                testContainer = testContainer.replace("{state}", "succes");
                testContainer = testContainer.replace("{simbolo}", "O");
                break;
            case Fallada:
                testContainer = testContainer.replace("{state}", "failure");
                testContainer = testContainer.replace("{simbolo}", "X");
                break;
            case Alerta:
                testContainer = testContainer.replace("{state}", "alert");
                testContainer = testContainer.replace("{simbolo}", "!");
                break;
        }
        String tc = testContainer;
        tc = tc
            .replace("{nombre_prueba}", Encode(prueba.nombre_prueba))
            .replace("{duracion}", Encode(prueba.duracion))
            .replace("{descripcion}", Encode(prueba.descripcion));
   
        String content = "";
        for (int i = 0; i < prueba.resultados.size(); i++)
        {
            if (prueba.resultados.get(i).resultado == ResultadoPrueba.Resultado.Aprobada)
            {
                content = content.concat(detalleAprobada);
                continue;
            }

            String item = detallesImagen;
            item = item
                .replace("{detalles_salida}", Encode(prueba.resultados.get(i).detalles_salida)
                .replace(System.lineSeparator(), "<br />"))
                .replace("{imagen_prueba}", "cid:" + Encode(prueba.resultados.get(i).imagen_prueba));

            if (prueba.resultados.get(i).resultado == ResultadoPrueba.Resultado.Alerta)
            {
                item = item.replace("{state}", "alret");
            }
            else if (prueba.resultados.get(i).resultado == ResultadoPrueba.Resultado.Fallada)
            {
                item = item.replace("{state}", "failure");
            }
            content = content.concat(item);
        }
        tc = tc.replace("{content}", content);
        return tc;
    }

    /// <summary>
    /// Este metodo se engarga de recopilar la informacion relevante para la generacion del html y generarlo
    /// </summary>
    public static String GenerarReporteEmail(HashMap<Navegador, ResultadoPrueba[]> listadoDeResultados, String titulo, String duration)
    {
        String fecha = Utils.GetDate();

        int cantPruebasTotales = 0;
        for (Navegador nav : listadoDeResultados.keySet())
        {
            for (int i = 0; i < listadoDeResultados.get(nav).length; i++)
            {
                cantPruebasTotales++;
            }
        }
        String doc;
        if (cantPruebasTotales == 0)
        {
            doc = GenerarHTMLIndividual(
                titulo,
                fecha,
                listadoDeResultados.get(new ArrayList<Navegador>(listadoDeResultados.keySet()).get(0))[0]);
        }
        else
        {
            int aprobadas = 0;
            int falladas = 0;
            int alerta = 0;
            for (Navegador nav : listadoDeResultados.keySet())
            {
                for (int i = 0; i < listadoDeResultados.get(nav).length; i++)
                {
                    switch (listadoDeResultados.get(nav)[i].resultado)
                    {
                        case Aprobada: aprobadas++; break;
                        case Fallada:  falladas++;  break;
                        case Alerta:   alerta++;    break;
                        default: break;
                    }
                }
            }

            float indiceAprobadas = (float)aprobadas / (float)cantPruebasTotales;
            float indiceFalladas = (float)falladas / (float)cantPruebasTotales;
            float indiceAlerta = (float)alerta / (float)cantPruebasTotales;
            int grados_aprobadas = (int)(indiceAprobadas * 100);
            int grados_falladas = (int)(indiceFalladas * 100);
            int grados_alerta = (int)(indiceAlerta * 100);

            doc = GenerarHTMLMultiple(
                titulo,
                fecha,
                String.valueOf(grados_aprobadas),
                String.valueOf(grados_falladas),
                String.valueOf(grados_alerta),
                String.valueOf(aprobadas),
                String.valueOf(falladas),
                String.valueOf(alerta),
                String.valueOf(listadoDeResultados.size()),
                duration,
                listadoDeResultados);
        }
        return doc;
    }

    public static String Encode(String string){
        return string
        .replace("<", "&lt;")
        .replace(">", "&gt;")
        .replace("¡", "&iexcl;")
        .replace("\"", "&quot;")
        .replace("&", "&amp;");
    }
}
