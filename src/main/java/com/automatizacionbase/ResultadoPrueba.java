package com.automatizacionbase;

import java.util.ArrayList;

/// <summary>
    /// Clase que contiene informacion relevante al resultado de una prueba
    /// </summary>
    public class ResultadoPrueba
    {
        public enum Resultado { Aprobada, Fallada, Alerta}
        public String nombre_prueba = "";
        public String descripcion = "";

        public Resultado resultado = Resultado.Aprobada;
        public String duracion = "";
        public String detalles_salida = "";
        public String imagen_prueba = "";

        public ArrayList<ResultadoIndividual> resultados = new ArrayList<ResultadoIndividual>();


        public ResultadoPrueba(String nombre_prueba, String descripcion)
        {
            this.nombre_prueba = nombre_prueba;
            this.descripcion = descripcion;
            this.resultados = new ArrayList<ResultadoIndividual>();
        }
        public ResultadoPrueba(Resultado aprobada, String nombre_prueba, String descripcion, String duracion, String detalles_salida)
        {
            this.resultado = aprobada;
            this.nombre_prueba = nombre_prueba;
            this.descripcion = descripcion;
            this.duracion = duracion;
            this.detalles_salida = detalles_salida;
            this.resultados = new ArrayList<ResultadoIndividual>();

        }
        public void AgregarResultado(Resultado resultado, String duracion, String detalles_salida, String imagen_prueba)
		{
            resultados.add(new ResultadoIndividual(resultado,duracion,detalles_salida,imagen_prueba));
		}

        public class ResultadoIndividual {
            public Resultado resultado = Resultado.Aprobada;
            public String duracion = "";
            public String detalles_salida = "";
            public String imagen_prueba = "";

            public ResultadoIndividual(Resultado resultado, String duracion, String detalles_salida, String imagen_prueba){
                this.resultado = resultado;
                this.duracion = duracion;
                this.detalles_salida = detalles_salida;
                this.imagen_prueba = imagen_prueba;
            }
        }
    }
