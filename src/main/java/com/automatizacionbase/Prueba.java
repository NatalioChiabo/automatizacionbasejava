package com.automatizacionbase;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Prueba
{
    public String nombre_prueba;
    public String descripcion;
    public Method metodo_prueba;

    public Prueba(String nombre_prueba, Method metodo_prueba)
    {
        this.nombre_prueba = nombre_prueba;
        this.metodo_prueba = metodo_prueba;
    }
    public Prueba(String nombre_prueba, String descripcion, Method metodo_prueba)
    {
        this.nombre_prueba = nombre_prueba;
        this.descripcion = descripcion;
        this.metodo_prueba = metodo_prueba;
    }
    public void RunMethod() throws PruebaFalladaException, IllegalAccessException, InvocationTargetException
    {
        metodo_prueba.invoke(null);
    }
}
