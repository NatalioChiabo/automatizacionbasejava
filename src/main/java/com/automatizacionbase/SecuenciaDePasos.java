package com.automatizacionbase;

import java.util.*;

/// <summary>
    /// Esta clase controla la secuencia de pasos
    /// Hay 3 tipos, Abrir, Cerrar y Agregar
    /// Abrir provoca que los siguientes pasos tengan un nivel mas de identacion
    /// Cerrar provoca que tanto este como los siguientes pasos tengan un nivel menos de identacion
    /// Agregar no produce cambios en la identacion
    /// 
    /// Idealmente Cada paso Abrir deberia tener un paso Cerrar de contraparte y que este este al final de la accion
    /// </summary>
    public class SecuenciaDePasos
    { 
        public List<String> secuencia = new ArrayList<String>();
        public int identacion = 0;
        public void AbrirPaso(String mensaje)
        {
            String msg = AgregarIdentacion(mensaje, "+");
            System.out.println(msg);
            secuencia.add(msg);

            identacion++;
        }

        public void CerrarPaso(String mensaje)
        {
            identacion--;
			if (identacion < 0)
			{
                identacion = 0;
			}
            String msg = AgregarIdentacion(mensaje, "-");
            System.out.println(msg);
            secuencia.add(msg);
        }

        public void AgregarPaso(String mensaje)
        {
            String msg = AgregarIdentacion(mensaje, "*");
            System.out.println(msg);
            secuencia.add(msg);
        }

        public void Clear()
        {
            secuencia.clear();
            identacion = 0;
        }

        String AgregarIdentacion(String mensaje, String token)
        {
            String ident = repeat("----", identacion);
            mensaje = ident + token + mensaje;
            return mensaje;
        }

        String repeat(String s, int times){
            String result = "";
            for(int i = 0; i < times; i++){
                result += s;
            }
            return result;
        }
    }
