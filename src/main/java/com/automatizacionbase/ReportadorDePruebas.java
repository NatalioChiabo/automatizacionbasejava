package com.automatizacionbase;

import java.io.*;
import java.util.*;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriverException;

public class ReportadorDePruebas {
    /// <summary>
    /// Esto es el listado de todas las pruebas
    /// Cada prueba tiene un nombre y una referencia al metodo de inicio.
    /// </summary>
    public static Prueba[] todas_las_pruebas = { };

    /// <summary>
    /// Esto es el listado de todas los navegadores para los cuales realizar un prueba
    /// usar muchos navegadores causara que las pruebas tarden mucho mas en terminar ya que cada prueba tiene que ejecutarse en cada navegador
    /// </summary>
    public static Navegador[] navegadores_a_usar = new Navegador[]
    {
        Navegador.Chrome
    };

    public static int threadLimit = 3;
    static int threadCount = 0;
    static int testsLeft;

    public static ThreadLocal<Navegador> navegador;

    public static SecuenciaDePasos secuenciaDePasos;

    /// <summary>
    /// Correra las pruebas y generara el reporte sin ejecutarlo para que puedas mandarlo por mail
    /// </summary>
    /// <param name="threading">Ejecutar multiple pruebas en paralelo, puede causar problemas si se utilizan los mismos usuarios</param>
    public static void CorrerPruebas(String titulo, Prueba[] pruebas, Boolean threading, Boolean open_email)
    {
        BorrarImagenes();

        Tuple<HashMap<Navegador, ResultadoPrueba[]>,String> resultado;
        if (threading && pruebas.length > 1)
        {
            resultado = CorrerPruebasEnParalelo(pruebas);
        }
        else
        {
            resultado = CorrerPruebasUnHilo(pruebas);
        }

        String doc = EmailManager.GenerarReporteEmail(resultado.item1, titulo, resultado.item2);
        if (open_email)
        {
            doc = doc.replace("cid:", "..\\Capturas\\");
        }
        
        try {

            File file = new File(System.getProperty("user.dir") + "\\HTML\\Email_Output.html");
            file.createNewFile();

            FileWriter myWriter = new FileWriter(file);
            myWriter.write(doc);
            myWriter.close();
            if (open_email)
            {
                File f = new File(System.getProperty("user.dir") + "\\HTML\\Email_Output.html");
                java.awt.Desktop.getDesktop().open(f);
            }
        } catch (IOException e) {
            
            e.printStackTrace();
        }
    }

    /// <summary>
    /// Este metodo corre todas las pruebas dadas.
    /// Si la prueba se completa sin error se llama el metodo PasarPrueba()
    /// Si durante la prueba ocurre algun error, si no se encontrase algun elemento web, o si una comprobacion fallase
    /// entonces se tirara la excepcion 'PruebaFalladaException' que sera atrapada por el try/catch y procedera a FallarPrueba()
    /// </summary>
    static Tuple<HashMap<Navegador, ResultadoPrueba[]>,String> CorrerPruebasUnHilo(Prueba[] pruebas)
    {
        HashMap<Navegador, ResultadoPrueba[]> listadoDeResultados = new HashMap<Navegador, ResultadoPrueba[]>();
        long startTime = System.currentTimeMillis();
        
        
        for (Navegador nav : navegadores_a_usar) {
            listadoDeResultados.put(nav, new ResultadoPrueba[pruebas.length]);

            for (int i = 0; i < pruebas.length; i++)
            {
                listadoDeResultados.get(nav)[i] = CorrerPruebaIndividual(pruebas[i], nav); ;
            }
        }
        long endTime = System.currentTimeMillis();
        
        String duration = String.valueOf(endTime - startTime);
        return new Tuple<HashMap<Navegador,ResultadoPrueba[]>,String>(listadoDeResultados, duration);
    }

    /// <summary>
    /// Este metodo corre todas las pruebas dadas en paralelo.
    /// Si la prueba se completa sin error se llama el metodo PasarPrueba()
    /// Si durante la prueba ocurre algun error, si no se encontrase algun elemento web, o si una comprobacion fallase
    /// entonces se tirara la excepcion 'PruebaFalladaException' que sera atrapada por el try/catch y procedera a FallarPrueba()
    /// </summary>
    static Tuple<HashMap<Navegador, ResultadoPrueba[]>,String> CorrerPruebasEnParalelo(Prueba[] pruebas)
    {
        HashMap<Navegador, ResultadoPrueba[]> listadoDeResultados = new HashMap<Navegador, ResultadoPrueba[]>();
        
        long startTime = System.currentTimeMillis();

        testsLeft = pruebas.length * navegadores_a_usar.length;
        for (Navegador nav : navegadores_a_usar)
        {
            listadoDeResultados.put(nav, new ResultadoPrueba[pruebas.length]);
            for (int i = 0; i < pruebas.length; i++)
            {
                while (threadCount >= threadLimit)
                {
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
                threadCount++;

                int cashedIndex = i;


                Thread testThread = new Thread(() ->
                {
                    listadoDeResultados.get(nav)[cashedIndex] = CorrerPruebaIndividual(pruebas[cashedIndex], nav);
                    threadCount--;
                    testsLeft--;
                });
                testThread.start();
            }
        }

        while (testsLeft > 0)
        {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        long endTime = System.currentTimeMillis();
        
        String duration = String.valueOf(endTime - startTime);
        return new Tuple<HashMap<Navegador,ResultadoPrueba[]>,String>(listadoDeResultados, duration);
    }

    /// <summary>
    /// Este metodo corre la prueba indicada.
    /// </summary>
    static ResultadoPrueba CorrerPruebaIndividual(Prueba prueba, Navegador d)
    {
        if(navegador == null){
            navegador = new ThreadLocal<Navegador>();
        }
        navegador.set(d);
        secuenciaDePasos = new SecuenciaDePasos();
        ResultadoPrueba pruebaActual = new ResultadoPrueba(prueba.nombre_prueba, prueba.descripcion);

        long startTime = System.currentTimeMillis();
        long endTime = System.currentTimeMillis();
        int totalTries = 3;

        for (int tries = 1; tries <= totalTries; tries++)
        {

            secuenciaDePasos.Clear();
            try
            {
                startTime = System.currentTimeMillis();

                secuenciaDePasos.AbrirPaso(prueba.nombre_prueba);

                switch (d)
                {
                    case Chrome:
                        Driver.IniciarChromeDriver();
                        break;
                    case Firefox:
                        Driver.IniciarFireFoxDriver();
                        break;
                    case IE:
                        Driver.IniciarIEDriver();
                        break;
                }

                prueba.RunMethod();

                endTime = System.currentTimeMillis();


                Driver.AlTerminarPrueba();

                PasarPrueba(pruebaActual, String.valueOf(endTime-startTime));

                Driver.Quit();
                secuenciaDePasos.Clear();
                //System.out.
                break;
            }
            catch (PruebaFalladaException ex)
            {
                endTime = System.currentTimeMillis();

                Driver.AlTerminarPrueba();
                FallarPrueba(pruebaActual, ex.getMessage(), String.valueOf(endTime-startTime));

                Driver.Quit();
                secuenciaDePasos.Clear();
                break;
            }
            catch (WebDriverException ex)
            {                    
                endTime = System.currentTimeMillis();

                Driver.AlTerminarPrueba();

                if (tries < totalTries)
                {
                    secuenciaDePasos.AgregarPaso("");
                    secuenciaDePasos.AgregarPaso("Reintentando despues de error de selenium: " + ex.getMessage());
                    secuenciaDePasos.AgregarPaso("");

                }

                AlertarPrueba(pruebaActual,
                    System.lineSeparator()  +
                    "Excepcion de selenium no esperada." +
                    System.lineSeparator()  +
                    System.lineSeparator()  +
                    ex.getMessage() +
                    System.lineSeparator()  +
                    ex.getStackTrace(), String.valueOf(endTime-startTime));

                Driver.Quit();
                secuenciaDePasos.Clear();
            }
            catch (Exception ex)
            {
                endTime = System.currentTimeMillis();

                Driver.AlTerminarPrueba();

                if (tries < totalTries)
                {
                    secuenciaDePasos.AgregarPaso("");
                    secuenciaDePasos.AgregarPaso("Reintentando despues de error inesperado: " + ex.getMessage());
                    secuenciaDePasos.AgregarPaso("");
                }

                FallarPrueba(pruebaActual,
                System.lineSeparator() +
                    "Excepcion no esperada." +
                    System.lineSeparator() +
                    System.lineSeparator()+
                    ex.getMessage() +
                    System.lineSeparator() +
                    ex.getStackTrace(), String.valueOf(endTime-startTime));

                Driver.Quit();
                secuenciaDePasos.Clear();
            }

        }
        secuenciaDePasos.CerrarPaso(prueba.nombre_prueba);

        secuenciaDePasos.Clear();

        return pruebaActual;
    }

    
        /// <summary>
        /// Este metodo se ejecuta cuando una prueba falla
        /// Se encarga de recopilar la informacion relevante a la prueba (la secuencia de pasos, el mensaje de error, duracion, etc)
        /// Sacar una imagen del momento en el que fallo la prueba
        /// Cerrar el WebDriver
        /// Etc
        /// </summary>
        static void FallarPrueba(ResultadoPrueba prueba, String detalles_salida, String duracion)
        {
            String nombre = prueba.nombre_prueba.replace(" ", "_");
            String imagen_prueba = nombre + "_" + Utils.GetDate();

            ImprimirImagen(imagen_prueba);

            String detalles = "";
            for (int i = 0; i < secuenciaDePasos.secuencia.size(); i++)
            {
                prueba.detalles_salida += secuenciaDePasos.secuencia.get(i) + System.lineSeparator();
                detalles += secuenciaDePasos.secuencia.get(i) + System.lineSeparator();
            }

            prueba.resultado = ResultadoPrueba.Resultado.Fallada;
            prueba.detalles_salida += detalles_salida;
            detalles += detalles_salida;
            prueba.imagen_prueba = imagen_prueba;
            prueba.duracion = duracion;

            prueba.AgregarResultado(ResultadoPrueba.Resultado.Fallada, duracion, detalles, imagen_prueba);
        }

        /// <summary>
        /// Este metodo se ejecuta cuando una prueba falla por un error fuera de el proyecto
        /// Se encarga de recopilar la informacion relevante a la prueba (la secuencia de pasos, el mensaje de error, duracion, etc)
        /// Sacar una imagen del momento en el que fallo la prueba
        /// Cerrar el WebDriver
        /// Etc
        /// </summary>
        static void AlertarPrueba(ResultadoPrueba prueba, String detalles_salida, String duracion)
        {
            String nombre = prueba.nombre_prueba.replace(" ", "_");
            String imagen_prueba = nombre + "_" + Utils.GetDate();

            ImprimirImagen(imagen_prueba);
            String detalles = "";

            for (int i = 0; i < secuenciaDePasos.secuencia.size(); i++)
            {
                prueba.detalles_salida += secuenciaDePasos.secuencia.get(i) + System.lineSeparator();
                detalles += secuenciaDePasos.secuencia.get(i) + System.lineSeparator();
            }

            prueba.resultado = ResultadoPrueba.Resultado.Alerta;
            prueba.detalles_salida += detalles_salida;
            detalles += detalles_salida;
            prueba.imagen_prueba = imagen_prueba;
            prueba.duracion = duracion;

            prueba.AgregarResultado(ResultadoPrueba.Resultado.Alerta, duracion, detalles, imagen_prueba);
        }


        /// <summary>
        /// Este metodo se ejecuta cuando una prueba pasa
        /// Una prueba exitosa no necesita demasiada informacion, solo la duracion
        /// Cerrar el WebDriver
        /// </summary>
        static void PasarPrueba(ResultadoPrueba prueba, String duracion)
        {
            prueba.resultado = ResultadoPrueba.Resultado.Aprobada;
            prueba.duracion = duracion;

            prueba.AgregarResultado(ResultadoPrueba.Resultado.Aprobada, duracion, "", "");
        }

    /// <summary>
    /// Este metodo Imprime la imagen actual del WebDriver
    /// </summary>
    static Boolean ImprimirImagen(String titulo)
    {
        try
        {
            Thread.sleep(2000);

            if (Driver.driver != null)
            {
                byte[] imagen = ((TakesScreenshot) Driver.driver).getScreenshotAs(OutputType.BYTES);

                OutputStream outputStream = new FileOutputStream(System.getProperty("user.dir") + "\\Capturas\\" + titulo);
                
                outputStream.write(imagen);
                outputStream.close();

                System.out.println("Image saved at:");
                System.out.println(System.getProperty("user.dir") + "\\Capturas\\" + titulo);
                return true;
            }
            return false;
        }
        catch (Exception ex)
        {
            System.out.println(ex.getMessage());
            return false;
        }
    }

    /// <summary>
    /// Este metodo borra todas las imagenes .jpg dentro de la carpeta 'Capturas', para no llenar el disco de cosas inutiles
    /// Debe ejecutarse antes de realizar pruebas, si estas se van a ver en el html o si se va a enviar por mail, se pueden borrar
    /// despues de esto
    /// </summary>
    static void BorrarImagenes()
    {
        System.out.println(System.getProperty("user.dir") + "\\Capturas\\");
        File[] images = new File(System.getProperty("user.dir") + "\\Capturas\\").listFiles(
            new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.toLowerCase().endsWith(".txt");
            }
        });
        for (int i = 0; i < images.length; i++)
        {
            System.out.println(images[i].getName());
            //new File(images[i].getName()).delete();
        }
    }
}


