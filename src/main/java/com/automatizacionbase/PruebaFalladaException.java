package com.automatizacionbase;

public class PruebaFalladaException extends Exception
{
    public PruebaFalladaException() { super();}
    public PruebaFalladaException(String message) { super(message); }
    public PruebaFalladaException(String message, Exception inner) {super(message, inner); }
}