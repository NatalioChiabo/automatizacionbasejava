package com.automatizacionbase;

import java.util.Calendar;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.*;

public class Utils {
    //Los pongo aca para facilmente ajustar los tiempos sin ir uno por uno
    public static int shortSleep = 800;
    public static int midSleep = 1000;
    public static int longSleep = 1200;
    public static int extraLongSleep = 3000;

    public static String GetDate(){
        Calendar now = Calendar.getInstance();
        String date =  String.format("%s_%s_%s__%s_%s_%s.jpg",
        now.get(Calendar.DAY_OF_MONTH),
        now.get(Calendar.MONTH) + 1,
        now.get(Calendar.YEAR),
        now.get(Calendar.HOUR_OF_DAY),
        now.get(Calendar.MINUTE),
        now.get(Calendar.SECOND));
        return date;
    }

    public static WebElement FindElement(By by) throws PruebaFalladaException
    {
        try
        {
            WaitFor(by, 10, 500);
            WebElement element = Driver.driver.findElement(by);
            return element;
        }
        catch (NotFoundException ex)
        {
            throw new PruebaFalladaException("No se encontro el elemento: " + by.toString());
        }
    }

    /// <summary>
    /// Busca un elemento, le hace clic y lo retorna.
    /// </summary>
    /// <param name="by">El By que representa al elemento al cual se le quiere hacer click</param>
    /// <returns>El elemento encontrado</returns>
    /// <exception cref="OpenQA.Selenium.NoSuchElementException">Si el elemento no se encuentra</exception>
    public static WebElement Click(By by) throws PruebaFalladaException
    {
        WebElement to_click = FindElement(by);
        new WebDriverWait(Driver.driver, 20000).until(ExpectedConditions.elementToBeClickable(by)).click();
        //to_click.Click();
        return to_click;
    }

    public static void ClickJS(String xpath)
    {
        String javascript = "document.evaluate( \"{xpath}\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue?.click()";
        JavascriptExecutor jsExecutor = (JavascriptExecutor)Driver.driver;
        jsExecutor.executeScript(javascript);
    }

    public static void ScrollIntoViewJS(WebElement element)
    {
        String javascript = "arguments[0].scrollIntoView(true);";
        JavascriptExecutor jsExecutor = (JavascriptExecutor)Driver.driver;
        jsExecutor.executeScript(javascript,element);
    }

    /// <summary>
    /// Busca un elemento, le manda el texto y lo retorna.
    /// </summary>
    /// <param name="by">El By que representa al elemento al cual se le quiere enviar texto</param>
    /// <returns>El elemento encontrado</returns>
    /// <exception cref="OpenQA.Selenium.NoSuchElementException">Si el elemento no se encuentra</exception>
    public static WebElement SendKeys( By by, String keys) throws PruebaFalladaException
    {
        WebElement to_send = FindElement(by);
        try
        {
            to_send.sendKeys(keys);
        }
        catch (ElementNotInteractableException ex)
        {
            try {
                Thread.sleep(longSleep);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            try
            {
                to_send.sendKeys(keys);
            }
            catch (ElementNotInteractableException ex2)
            {
                throw ex2;
            }
        }
        return to_send;

    }

    /// <summary>
    /// Busca un elemento, limpia el texto actual, le manda el texto y lo retorna.
    /// </summary>
    /// <param name="by">El By que representa al elemento al cual se le quiere enviar texto</param>
    /// <returns>El elemento encontrado</returns>
    /// <exception cref="OpenQA.Selenium.NoSuchElementException">Si el elemento no se encuentra</exception>
    public static WebElement ClearSendKeys( By by, String keys)
    {
        WebElement to_send = new WebDriverWait(Driver.driver, 30000)
            .until(ExpectedConditions.elementToBeClickable(by));
        try
        {
            to_send.clear();
            to_send.sendKeys(keys);
        }
        catch (ElementNotInteractableException ex )
        {
            try {
                Thread.sleep(longSleep);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            try
            {
                to_send.clear();
                to_send.sendKeys(keys);
            }
            catch (ElementNotInteractableException ex2)
            {
                throw ex2;
            }
        }
        return to_send;
    }

    /// <summary>
    /// Busca un checkbox, le hace click si fuese necesario y lo retorna.
    /// </summary>
    /// <param name="by">El By que representa al checkbox al cual se quiere clickear</param>
    /// <returns>El elemento encontrado</returns>
    /// <exception cref="OpenQA.Selenium.NoSuchElementException">Si el elemento no se encuentra</exception>
    public static WebElement Checkbox(By by, Boolean value)
    {
        WebElement checkbox = Driver.driver.findElement(by);
        if (checkbox.isSelected() != value)
        {
            checkbox.click();
        }
        return checkbox;
    }

    /// <summary>
    /// Busca un checkbox y comprueba si esta selecionado.
    /// </summary>
    /// <param name="by">El By que representa al checkbox al cual se le quiere comprobar el estado</param>
    /// <returns>El elemento encontrado</returns>
    /// <exception cref="OpenQA.Selenium.NoSuchElementException">Si el elemento no se encuentra</exception>
    public static Boolean IsChecked(By by) throws PruebaFalladaException
    {
        return FindElement(by).isSelected();
    }

    /// <summary>
    /// Busca un elemento y retorna el valor del atributo 'value'.
    /// </summary>
    /// <param name="by">El By que representa al elemento al cual se le quiere obtener los hijos</param>
    /// <returns>El elemento encontrado</returns>
    /// <exception cref="OpenQA.Selenium.NoSuchElementException">Si el elemento no se encuentra</exception>
    public static String GetValue(By by) throws PruebaFalladaException
    {
        return FindElement(by).getAttribute("value");
    }

    /// <summary>
    /// Busca un elemento y retorna el valor del atributo.
    /// </summary>
    /// <param name="by">El By que representa al elemento al cual se le quiere obtener los hijos</param>
    /// <returns>El elemento encontrado</returns>
    /// <exception cref="OpenQA.Selenium.NoSuchElementException">Si el elemento no se encuentra</exception>
    public static String GetAttribute(By by, String attribute) throws PruebaFalladaException
    {
        return FindElement(by).getAttribute(attribute);
    }

    /// <summary>
    /// Busca un elemento y retorna el texto que emcompasa.
    /// </summary>
    /// <param name="by">El By que representa al elemento al cual se le quiere obtener los hijos</param>
    /// <returns>El elemento encontrado</returns>
    /// <exception cref="OpenQA.Selenium.NoSuchElementException">Si el elemento no se encuentra</exception>
    public static String GetText(By by) throws PruebaFalladaException
    {
        return FindElement(by).getText();
    }

    /// <summary>
    /// Devuelve los hijos del elemento representado por el By
    /// </summary>
    /// <param name="by">El By que representa al elemento al cual se le quiere obtener los hijos</param>
    /// <returns>Retorna un array con todoslos hijos</returns>
    public static WebElement[] GetChildren(By by) throws PruebaFalladaException
    {
        WebElement parent = FindElement(by);
        return GetChildren(parent);
    }

    /// <summary>
    /// Devuelve los hijos del elemento parent
    /// </summary>
    /// <param name="parent">Elemento al cual se le quiere obtener los hijos</param>
    /// <returns>Retorna un array con todoslos hijos</returns>
    public static WebElement[] GetChildren(WebElement parent)
    {
        List<WebElement> coleccion = parent.findElements(By.xpath("./child::*"));

        WebElement[] children = new WebElement[coleccion.size()];
        children = (WebElement[]) coleccion.toArray();

        return children;
    }

    /// <summary>
    /// Busca el dropdown representado por el by y elige el item definido por el texto
    /// </summary>
    /// <param name="text">El texto usado para elegir el elemento deseado</param>
    /// <param name="by">Objeto de tipo By que representa el Dropdown</param>
    /// <param name="partialMatch">Si es falso el texto debera ser exacto, si no se realizara una comprobacion parcial</param>
    /// <returns>El SelectElement creado con el elemento encontrado</returns>
    /// <exception cref="OpenQA.Selenium.NoSuchElementException">Cuando no encuentra el elemento</exception>
    /// <exception cref="OpenQA.Selenium.Support.UI.UnexpectedTagNameException">Cuando el elemento no es del tipo apropiado</exception>
    public static Select DropdownSelectText( By by, String text) throws PruebaFalladaException
    {
        WebElement elem = FindElement(by);
        Select select = new Select(elem);
        try
        {
            select.selectByVisibleText(text);
            return select;
        }
        catch (NoSuchElementException ex)
        {
            throw new PruebaFalladaException("No se encontro el item con el texto " + text);
        }
    }

    /// <summary>
    /// Busca el dropdown representado por el by y elige el item definido por el texto
    /// </summary>
    /// <param name="index">El indice del elemento deseado</param>
    /// <param name="by">Objeto de tipo By que representa el Dropdown</param>
    /// <returns>El SelectElement creado con el elemento encontrado</returns>
    /// <exception cref="OpenQA.Selenium.NoSuchElementException">Cuando no encuentra el elemento</exception>
    /// <exception cref="OpenQA.Selenium.Support.UI.UnexpectedTagNameException">Cuando el elemento no es del tipo apropiado</exception>
    public static Select DropdownSelectIndex(By by, int index) throws PruebaFalladaException
    {
        WebElement elem = FindElement(by);
        Select select = new Select(elem);
        
        try
        {
            select.selectByIndex(index);
            return select;
        }
        catch (NoSuchElementException ex )
        {
            throw new PruebaFalladaException("No se encontro el item con el indice " + index);
        }
    }


    /// <summary>
    /// Atajo para Assert.AreEqual en el que se realiza dentro de un try y en caso de causar una excepcion cierra el WebDriver
    /// </summary>
    /// <param name="esperado">El objeto esperado</param>
    /// <param name="actual">El objeto obtenido</param>
    /// <param name="mensaje">El mensaje a mostrar</param>
    /// <exception cref="AssertFailedException">Cuando no se satisface la igualdad</exception>
    public static void TryAssertEqual(Object esperado, Object actual, String mensaje) throws PruebaFalladaException
    {
        if (!actual.equals(esperado))
        {
            throw new PruebaFalladaException(mensaje);
        }
    }

    /// <summary>
    /// Atajo para Assert.IsFalse en el que se realiza dentro de un try y en caso de causar una excepcion cierra el WebDriver
    /// </summary>
    /// <param name="condicion">La condicion a evaluar</param>
    /// <param name="mensaje">El mensaje a mostrar</param>
    /// <exception cref="AssertFailedException">Cuando la condicion es falsa</exception>
    public static void TryAssertFalse(Boolean condicion, String mensaje) throws PruebaFalladaException
    {
        if (!condicion)
        {
            throw new PruebaFalladaException(mensaje);
        }
    }

    /// <summary>
    /// Comprueba si un elemento existe
    /// </summary>
    /// <param name="element">El By que representa al elemento al cual se le quiere hacer click</param>
    /// <returns>Un Boolean representando si el elemento existe o no</returns>
    public static Boolean ElementExists( By element)
    {
        Driver.driver.manage().timeouts().implicitlyWait(0,TimeUnit.MILLISECONDS);
        Boolean exists = Driver.driver.findElements(element).size() != 0;
        Driver.driver.manage().timeouts().implicitlyWait(30000,TimeUnit.MILLISECONDS);
        return exists;
    }

    /// <summary>
    /// Busca un elemento y los devuelve si existe, si no existe devuelve null
    /// </summary>
    /// <param name="element">El By que representa al elemento al cual se le quiere hacer click</param>
    /// <returns>El elemento buscado o null</returns>
    public static WebElement ElementExistsGet(By element)
    {            
        Driver.driver.manage().timeouts().implicitlyWait(0,TimeUnit.MILLISECONDS);
        List<WebElement> elems = Driver.driver.findElements(element);
        Driver.driver.manage().timeouts().implicitlyWait(30000,TimeUnit.MILLISECONDS);
        if (elems.size() != 0)
        {
            return elems.get(0);
        }
        return null;
    }

    /// <summary>
    /// Espera a que un elemento se cierre o a que otro aparezca. A mas intentos y menor delay se realizaran mas comprobaciones en el mismo tiempo
    /// </summary>
    /// <param name="toClose">El By que representa al elemento a cerrarse</param>
    /// <param name="toApear">El By que representa al elemento a aparecer</param>
    /// <param name="toClick">El By que representa al elemento a clickear si el elemento 'toApear' aparece</param>
    /// <param name="tries">Los intentos a realziar</param>
    /// <param name="sleep">El tiempo entre intentos</param>
    public static void WaitForCloseOrApear(By toClose, By toApear, By toClick, int tries, int sleep) throws PruebaFalladaException
    {
        while (tries > 0)
        {
            if (ElementExists(toClose))
            {
                break;
            }
            else
            {
                if (ElementExists(toApear))
                {
                    Sleep(sleep);
                    Click(toClick);
                    break;
                }
            }
            Sleep(sleep);
            tries--;
        }
        
    }

    /// <summary>
    /// Espera a que un elemento aparezca o a que otro se cierre. A mas intentos y menor delay se realizaran mas comprobaciones en el mismo tiempo
    /// </summary>
    /// <param name="toClose">El By que representa al elemento a cerrarse</param>
    /// <param name="toApear">El By que representa al elemento a aparecer</param>
    /// <param name="toClick">El By que representa al elemento a clickear si el elemento 'toClose' se cierra</param>
    /// <param name="tries">Los intentos a realziar</param>
    /// <param name="sleep">El tiempo entre intentos</param>
    public static void WaitForApearOrClose(By toClose, By toApear, By toClick, int tries, int sleep) throws PruebaFalladaException
    {
        while (tries > 0)
        {
            if (ElementExists(toApear))
            {
                break;
            }
            else
            {
                if (ElementExists(toClose))
                {
                    Sleep(sleep);
                    Click(toClick);
                    break;
                }
            }
            Sleep(sleep);
            tries--;
        }
        Sleep(sleep);
    }

    /// <summary>
    /// Espera a que un elemento aparezca. A mas intentos y menor delay se realizaran mas comprobaciones en el mismo tiempo
    /// </summary>
    /// <param name="toApear">El By que representa al elemento a aparecer</param>
    /// <param name="toClick">El By que representa al elemento a clickear si el elemento 'toClose' se cierra</param>
    /// <param name="tries">Los intentos a realziar</param>
    /// <param name="sleep">El tiempo entre intentos</param>
    public static void WaitForApear(By toApear, By toClick, int tries, int sleep) throws PruebaFalladaException
    {
        while (tries > 0)
        {
            if (ElementExists(toApear))
            {
                Sleep(sleep);
                Click(toClick);
                break;
            }
            Sleep(sleep);
            tries--;
        }
        Sleep(sleep);
    }

    /// <summary>
    /// Espera a que un elemento aparezca. A mas intentos y menor delay se realizaran mas comprobaciones en el mismo tiempo
    /// </summary>
    /// <param name="toApear">El By que representa al elemento a aparecer</param>
    /// <param name="tries">Los intentos a realziar</param>
    /// <param name="sleep">El tiempo entre intentos</param>
    public static Boolean WaitFor(By toApear, int tries, int sleep)
    {
        while (tries > 0)
        {
            if (ElementExists(toApear))
            {
                return true;
            }
            Sleep(sleep);
            tries--;
        }
        return false;
    }

    /// <summary>
    /// Espera a que un elemento aparezca. A mas intentos y menor delay se realizaran mas comprobaciones en el mismo tiempo
    /// </summary>
    /// <param name="toClose">El By que representa al elemento a aparecer</param>
    /// <param name="toClick">El By que representa al elemento a clickear si el elemento 'toClose' se cierra</param>
    /// <param name="tries">Los intentos a realziar</param>
    /// <param name="sleep">El tiempo entre intentos</param>
    public static void WaitForClose(By toClose, By toClick, int tries, int sleep) throws PruebaFalladaException
    {
        while (tries > 0)
        {
            if (ElementExists(toClose))
            {
                Sleep(sleep);
                Click(toClick);
                break;
            }
            Sleep(sleep);
            tries--;
        }
        Sleep(sleep);
    }

    /// <summary>
    /// Genera un Cuil basado en un dni generazo al azar
    /// </summary>
    public static String GenerarCuil()
    {
        Random r = new Random();
        int dni = r.nextInt(10000000, 99999999);
        String dni_s = String.valueOf(dni);

        return CalcularCuil(dni_s);
    }

    /// <summary>
    /// Genera un Cuil basado en un dni generazo al azar
    /// </summary>
    public static String CalcularCuil( String dni )
    {
        int x = 2;
        int y = 0;
        int nx = 10;
        int ny = 0;
        int n1 = Integer.parseInt(String.valueOf(dni.charAt(0))) * 3;
        int n2 = Integer.parseInt(String.valueOf(dni.charAt(1))) * 2;
        int n3 = Integer.parseInt(String.valueOf(dni.charAt(2))) * 7;
        int n4 = Integer.parseInt(String.valueOf(dni.charAt(3))) * 6;
        int n5 = Integer.parseInt(String.valueOf(dni.charAt(4))) * 5;
        int n6 = Integer.parseInt(String.valueOf(dni.charAt(5))) * 4;
        int n7 = Integer.parseInt(String.valueOf(dni.charAt(6))) * 3;
        int n8 = Integer.parseInt(String.valueOf(dni.charAt(7))) * 2;

        int sum = nx + ny + n1 + n2 + n3 + n4 + n5 + n6 + n7 + n8;
        int z;
        float rest = (float)sum / 11f;
        if (rest == 0f)
        {
            z = 0;
        }
        else if (rest == 1)
        {
            z = 9;
            y = 3;
        }
        else
        {
            int temp = sum - Math.round((rest - rest % 1) * 11f);
            z = 11 - temp;
        }

        return String.valueOf(x) + String.valueOf(y) + dni + String.valueOf(z);
    }

    public static void Sleep( int miliseconds){
        try {
            Thread.sleep(miliseconds);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
