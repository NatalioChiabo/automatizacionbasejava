package com.automatizacionbase;

public class Tuple<S1, S2> {
    S1 item1;
    S2 item2;
    public Tuple(S1 item1, S2 item2){
        this.item1 = item1;
        this.item2 = item2;
    }
}
